export default class Game {
  constructor(gameData) {
    console.log(gameData)
    if (gameData !== null && Object.keys(gameData).length !== 0) {
      let date = gameData.Date.split('.');
      this.gameId = gameData.Game;
      this.dateTime = new Date(date[2] + '-' + date[1] + '-' + date[0] + 'T' + gameData.StartTime + ':00');
      this.homeId = gameData.homeID;
      this.guestId = gameData.guestID;
      this.league = gameData.CompName;
      this.leagueIdentifier = gameData.CompName.replace(' ', '-').toLowerCase();
      this.round = gameData.Round;
      this.coach = gameData.coach;
      this.teamroster = gameData.teamroster;
      this.roster = gameData.roster;
      this.referee = gameData.referee;
      this.site = gameData.Site;
    }
  }
}
