export default class Team {
  constructor(data) {
    this.teamCode = data.TC;
    this.name = data.TeamName;
    this.shortName = data.Team;
    this.teamId = data.TID;
    this.current_game_points = 0;
    this.current_game_ft_attempts = 0;
    this.current_game_ft_made = 0;
    this.current_game_ft_rate = 0;
    this.current_game_p2_attempts = 0;
    this.current_game_p2_made = 0;
    this.current_game_p2_rate = 0;
    this.current_game_p3_attempts = 0;
    this.current_game_p3_made = 0;
    this.current_game_p3_rate = 0;
    this.current_game_fg_attempts = 0;
    this.current_game_fg_made = 0;
    this.current_game_fg_rate = 0;
    this.current_game_fouls = 0;
    this.current_game_rebounds_offensive = 0;
    this.current_game_rebounds_defensive = 0;
    this.current_game_rebounds_total = 0;
    this.current_game_assists = 0;
    this.current_game_blocks = 0;
    this.current_game_steals = 0;
    this.current_game_turnover = 0;
    this.current_quarter_points = 0;
    this.current_quarter_ft_attempts = 0;
    this.current_quarter_ft_made = 0;
    this.current_quarter_ft_rate = 0;
    this.current_quarter_p2_attempts = 0;
    this.current_quarter_p2_made = 0;
    this.current_quarter_p2_rate = 0;
    this.current_quarter_p3_attempts = 0;
    this.current_quarter_p3_made = 0;
    this.current_quarter_p3_rate = 0;
    this.current_quarter_fg_attempts = 0;
    this.current_quarter_fg_made = 0;
    this.current_quarter_fg_rate = 0;
    this.current_quarter_fouls = 0;
    this.current_quarter_rebounds_offensive = 0;
    this.current_quarter_rebounds_defensive = 0;
    this.current_quarter_rebounds_total = 0;
    this.current_quarter_assists = 0;
    this.current_quarter_blocks = 0;
    this.current_quarter_steals = 0;
    this.current_quarter_turnover = 0;
    this.current_season_points = 0;
    this.current_season_ft_attempts = 0;
    this.current_season_ft_made = 0;
    this.current_season_ft_rate = 0;
    this.current_season_p2_attempts = 0;
    this.current_season_p2_made = 0;
    this.current_season_p2_rate = 0;
    this.current_season_p3_attempts = 0;
    this.current_season_p3_made = 0;
    this.current_season_p3_rate = 0;
    this.current_season_fg_attempts = 0;
    this.current_season_fg_made = 0;
    this.current_season_fg_rate = 0;
    this.current_season_fouls = 0;
    this.current_season_rebounds_offensive = 0;
    this.current_season_rebounds_defensive = 0;
    this.current_season_rebounds_total = 0;
    this.current_season_assists = 0;
    this.current_season_blocks = 0;
    this.current_season_steals = 0;
    this.current_season_turnover = 0;
  }

  setStats(stats) {
    this.current_game_ft_made = parseInt(stats[1] || 0);
    this.current_game_ft_attempts = parseInt(stats[2] || 0);
    this.current_game_ft_rate = parseInt(stats[3] || 0);
    this.current_game_p2_made = parseInt(stats[4] || 0);
    this.current_game_p2_attempts = parseInt(stats[5] || 0);
    this.current_game_p2_rate = parseInt(stats[6] || 0);
    this.current_game_p3_made = parseInt(stats[7] || 0);
    this.current_game_p3_attempts = parseInt(stats[8] || 0);
    this.current_game_p3_rate = parseInt(stats[9] || 0);
    this.current_game_fg_made = parseInt(stats[19] || 0);
    this.current_game_fg_attempts = parseInt(stats[20] || 0);
    this.current_game_fg_rate = parseInt(stats[21] || 0);
    this.current_game_fouls = parseInt(stats[10] || 0);
    this.current_game_rebounds_offensive = parseInt(stats[11] || 0);
    this.current_game_rebounds_defensive = parseInt(stats[12] || 0);
    this.current_game_rebounds_total = parseInt(stats[22] || 0);
    this.current_game_assists = parseInt(stats[23] || 0);
    this.current_game_blocks = parseInt(stats[13] || 0);
    this.current_game_steals = parseInt(stats[14] || 0);
    this.current_game_turnover = parseInt(stats[15] || 0);
    this.current_game_points = this.current_game_ft_made + this.current_game_p2_made * 2 + this.current_game_p3_made * 3;
  }

  setStatsFromActions(actions, teamCode) {
    this.current_quarter_points = 0;
    this.current_quarter_ft_attempts = 0;
    this.current_quarter_ft_made = 0;
    this.current_quarter_ft_rate = 0;
    this.current_quarter_p2_attempts = 0;
    this.current_quarter_p2_made = 0;
    this.current_quarter_p2_rate = 0;
    this.current_quarter_p3_attempts = 0;
    this.current_quarter_p3_made = 0;
    this.current_quarter_p3_rate = 0;
    this.current_quarter_fg_attempts = 0;
    this.current_quarter_fg_made = 0;
    this.current_quarter_fg_rate = 0;
    this.current_quarter_fouls = 0;
    this.current_quarter_rebounds_offensive = 0;
    this.current_quarter_rebounds_defensive = 0;
    this.current_quarter_rebounds_total = 0;
    this.current_quarter_assists = 0;
    this.current_quarter_blocks = 0;
    this.current_quarter_steals = 0;
    this.current_quarter_turnover = 0;

    actions.forEach(function (action) {
      if (action[0] === teamCode) {
        switch (action[4]) {
          case 'FT':
          case 'P2':
          case 'P3':
            this['current_quarter_' + action[4].toLowerCase() + '_attempts']++;
            if (action[8] === '+') {
              this['current_quarter_' + action[4].toLowerCase() + '_made']++;
            }
            action[3] !== '' ? this.current_quarter_assists++ : '';
            break;
          case 'REB':
          case 'TREB':
            this.current_quarter_rebounds_total ++;
            action[5] === 'D' ? this.current_quarter_rebounds_defensive++ : this.current_quarter_rebounds_offensive++
            break;
          case 'FOUL':
            this.current_quarter_fouls ++;
            break;
          case 'TO':
          case 'TTO':
            this.current_quarter_turnover ++;
            break;
          case 'ST':
            this.current_quarter_steals ++;
            break;
          case 'BL':
          case 'BS':
            this.current_quarter_blocks ++;
            break;
        }
      }
    }, this);

    this.current_quarter_fg_attempts = this.current_quarter_p2_attempts + this.current_quarter_p3_attempts;
    this.current_quarter_fg_made = this.current_quarter_p2_made + this.current_quarter_p3_made;
    if (this.current_quarter_ft_attempts > 0) {
      this.current_quarter_ft_rate = Math.round((this.current_quarter_ft_made / this.current_quarter_ft_attempts) * 100);
    }
    if (this.current_quarter_p2_attempts > 0) {
      this.current_quarter_p2_rate = Math.round((this.current_quarter_p2_made / this.current_quarter_p2_attempts) * 100);
    }
    if (this.current_quarter_p3_attempts > 0) {
      this.current_quarter_p3_rate = Math.round((this.current_quarter_p3_made / this.current_quarter_p3_attempts) * 100);
    }
    if (this.current_quarter_fg_attempts > 0) {
      this.current_quarter_fg_rate = Math.round((this.current_quarter_fg_made / this.current_quarter_fg_attempts) * 100);
    }

    this.current_quarter_points = this.current_quarter_ft_made + this.current_quarter_p2_made * 2 + this.current_quarter_p3_made * 3;
  }

  setSeasonStats(seasonStats) {
    if (seasonStats['g']) {
      const players = seasonStats['g'].filter(player => player.team_id === this.teamId);
      let absolute_number_of_games = 0;
      players.forEach(function (player) {
        this.current_season_points = this.current_season_points + parseInt(player.points)
        this.current_season_blocks = this.current_season_blocks + parseInt(player.BL_nr)
        this.current_season_steals = this.current_season_steals + parseInt(player.ST_nr)
        this.current_season_turnover = this.current_season_turnover + parseInt(player.TO_nr)
        this.current_season_assists = this.current_season_assists + parseInt(player.assists)
        this.current_season_rebounds_offensive = this.current_season_rebounds_offensive + parseInt(player.rbo)
        this.current_season_rebounds_defensive = this.current_season_rebounds_defensive + parseInt(player.rbd)
        this.current_season_rebounds_total = this.current_season_rebounds_total + parseInt(player.REB_nr)
        this.current_season_p2_attempts = this.current_season_p2_attempts + parseInt(player.P2_attempts);
        this.current_season_p2_made = this.current_season_p2_made + parseInt(player.P2_points);
        this.current_season_p3_attempts = this.current_season_p3_attempts + parseInt(player.P3_attempts);
        this.current_season_p3_made = this.current_season_p3_made + parseInt(player.P3_points);
        this.current_season_ft_attempts = this.current_season_ft_attempts + parseInt(player.FT_attempts);
        this.current_season_ft_made = this.current_season_ft_made + parseInt(player.FT_points);
        this.current_season_fouls = this.current_season_fouls + parseInt(player.fouls);
        absolute_number_of_games = parseInt(player.games) > absolute_number_of_games ? parseInt(player.games) : absolute_number_of_games;
      }, this, absolute_number_of_games)

      this.current_season_ft_rate = Math.round((this.current_season_ft_made / this.current_season_ft_attempts) * 100);
      this.current_season_ft_attempts = this.roundNumber(this.current_season_ft_attempts/absolute_number_of_games);
      this.current_season_ft_made = this.roundNumber(this.current_season_ft_made/absolute_number_of_games);

      this.current_season_fg_attempts = this.roundNumber((this.current_season_p2_attempts + this.current_season_p3_attempts)/absolute_number_of_games);
      this.current_season_fg_made = this.roundNumber((this.current_season_p2_made + this.current_season_p3_made)/absolute_number_of_games);
      this.current_season_fg_rate = Math.round(((this.current_season_p2_made + this.current_season_p3_made) / (this.current_season_p2_attempts + this.current_season_p3_attempts)) * 100);

      this.current_season_p2_rate = Math.round((this.current_season_p2_made / this.current_season_p2_attempts) * 100);
      this.current_season_p2_rate_string = this.roundNumber(this.current_season_p2_rate);
      this.current_season_p2_attempts = this.roundNumber(this.current_season_p2_attempts/absolute_number_of_games);
      this.current_season_p2_made = this.roundNumber(this.current_season_p2_made/absolute_number_of_games);

      this.current_season_p3_rate = Math.round((this.current_season_p3_made / this.current_season_p3_attempts) * 100);
      this.current_season_p3_rate_string = this.roundNumber(this.current_season_p3_rate);
      this.current_season_p3_attempts = this.roundNumber(this.current_season_p3_attempts/absolute_number_of_games);
      this.current_season_p3_made = this.roundNumber(this.current_season_p3_made/absolute_number_of_games);

      this.current_season_fouls = this.roundNumber(this.current_season_fouls / absolute_number_of_games);
      this.current_season_points = this.roundNumber(this.current_season_points / absolute_number_of_games);
      this.current_season_blocks = this.roundNumber(this.current_season_blocks / absolute_number_of_games);
      this.current_season_assists = this.roundNumber(this.current_season_assists / absolute_number_of_games);
      this.current_season_turnover = this.roundNumber(this.current_season_turnover / absolute_number_of_games);
      this.current_season_steals = this.roundNumber(this.current_season_steals / absolute_number_of_games);
      this.current_season_rebounds_defensive = this.roundNumber(this.current_season_rebounds_defensive / absolute_number_of_games);
      this.current_season_rebounds_offensive = this.roundNumber(this.current_season_rebounds_offensive / absolute_number_of_games);
      this.current_season_rebounds_total = this.roundNumber(this.current_season_rebounds_total / absolute_number_of_games);
    }

    return this;
  }

  roundNumber(number) {
    return ((Math.round(number * 10))/10).toLocaleString('de-DE');
  }
}
