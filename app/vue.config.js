module.exports = {

  publicPath: process.env.NODE_ENV === 'production'
    ? '/livestream-statistiken/'
    : '/',
  pages: {
    display: {
      entry: 'src/pages/display/main.js',
      title: 'Stats Display',
    },
    'control-board': {
      entry: 'src/pages/control-board/main.js',
      title: 'Stats Control Board',
    },
    'single-stats': {
      entry: 'src/pages/single-stats/main.js',
      title: 'Stats Single',
    },
  },
}
