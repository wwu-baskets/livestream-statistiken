# Basketball Live Statistiken für euren Livestream der ProA/ProB

Dieses Projekt soll für die Vereine der BARMER 2. Basketball Bundesliga eine einfache Möglichkeit bieten, Statistiken des aktuellen Spiels im Livestream einzublenden.

Ziel ist zudem, dies mit möglichst wenig zusätzlichem Aufwand am Spieltag zu realisieren. Datengrundlage ist die Eingabe des Scoutings, ähnlich wie beim offiziellen Liveticker.

Es stehen zwei Varianten der Nutzung zur Verfügung:

### Direkte integration in Vmix

Die allgemeinen Statistiken rund um den Spieltag können direkt über eine URL abgerufen werden. So können in Vmix mehrere Browser als Input vorbereitet werden, und dann als Overlay eingeblendet werden. Über Trigger muss konfiguriert werden, dass sichergestellt ist, dass der Browser bei Aufruf aktualisiert wird. Alternativ können über Skript Shortcuts erstellt werden, die dann auch animiertes Ausblenden der Statistiken ermöglichen.

Ein ähnliches Vorgehen ist auch mit OBS möglich.

### Steurung über ein Weboberfläche

Die Steuerung über eine separte Website ermöglicht einer anderen Person außerhalb der Regie die Einblendungen zu Steuern. Dazu wir im Schnittprogramm ein dauerhaftes Overlay eingeblendet, welches die Statistiken empfängt und anzeigt.

Die Statistiken auf der Weboberfläche werden dabei laufend aktualisiert.

Auf diesem Weg kann zum Beispiel den Kommentatoren die Möglichkeit gegeben werden, die Einblendungen selbst zu steuern. Gleichzeit enthält die Website alle Statistiken zum Spiel zum sofortigen Überblick, sodass während des Kommentieren schnell die passenden Statistiken gefunden werden können.

Diese Variante wird derzeit erfolgreich bei den Livestreams der WWU Baskets eingesetzt.

## Aktuelle Features
* Einblendung der Begegnungen des aktuellen Spieltags inkl. Live-Score
* Einblendungen der letzten zwei/nächsten Zwei Spiel der jeweiligen Teams
* Einblendungen der Kader je Team und der Starting Five
* Einblendungen von Team-Statistiken
  * Durchschnitte der Regulären Saison
  * Durchschnitte aktuelles Spiel bis zum aktuellen Zeitpunkt
  * Durchschnitte des aktuellen Viertels
* Boxscore je Heim und Auswärts
* Einblenden von Einzelspieler-Statistiken
  * Durchschnitte der Regulären Saison
  * Aktuelle Scoring-Statistiken
  * Aktuelle weitere Statistiken (Rebounds, Assits, etc.)

## Screenshots
* ![Tabelle](docs/spieltag.png "Spieltag")
* ![Tabelle](docs/tabelle.png "Tabelle")
* ![Team-Statistik](docs/teamstatistiken.png "Tabelle")
* ![Spieler](docs/spieler.png "Spieler")
* ![Spieler](docs/starting-five.png "Starting Five")
* ![Steuerung](docs/steuerung.png "Steuerung")

## Demo
[Demo Video](docs/demo.mp4 "Demo.mp4")

## Vorraussetzungen zur Nutzung
* Streaming-Software mit Unterstützung eines Internet-Browser overlays (VMIX, OBS)
   * Alternativ: Einblenden einer Bildschirmausgabe eines Computers mit Chroma-Key
* Gültiger API-Key der Liga zum Abruf der Statistiken. (Erhältlich auf Anfrage bei der Liga)
* Game-ID des aktuellen Spiels. Am besten auffindbar über die URL zum Liveticker. Aufgrund der Beschränkung des API-Keys können nur eigenen Heimspiele eingesetzt werden.
* Computer mit Internetanbindung zur Steuerung der Einblendungen. (Kann alternativ auch auf dem Streaming Rechner ausgeführt werden)
* Zugang zu einem Websocket-Server (aktuell nicht öffentlich verfügbar, kann auf Anfrage durch die WWU Baskets bereitgestellt werden)

## How-To
1. Legt euch die die URL's zurecht, nachdem ihr die benötigten Keys und URLs angefragt habt:
   1. Anzeige: `https://baskets.uber.space/livestream-statistiken/display.html?server=<URL_WEBSOCKET_SERVER>&key=<EUER_API_KEY>&background-color=transparent`
   2. Steuerung: `https://baskets.uber.space/livestream-statistiken/control-board.html?server=<URL_WEBSOCKET_SERVER>&key=<EUER_API_KEY>`
2. Richtet einen Browser-Overlay in euerer Streaming Software an, und tragt dort die Anzeige-URL ein. Auflösung sollte 1920x1080, also Full HD, betragen. Das Fenster kann dauerhaft eingeblendet bleiben.
3. Ruft auf einem zweiten Gerät die Steuerungs-URL auf. Dies kann ein beliebiges Gerät mit Internetanbindung sein. Dort klickt ihr auf "Einstellungen" und tragt die benötigten Daten ein und drückt auf "Speichern". Eventuell ist dann ein Neu-Laden der Seite notwending. Nun könnt ihr die Einblendungen steuern!
4. Zum Einblenden einer Statistik einen der blauen Buttons drücken. Dabei erscheint oben rechts ein roter Button "Clear Statistics", mit diesem kann die Anzeige gelöscht werden.

Beispiel im laufenden Betrieb (mit Münster spezifischem Design): (Vorbericht ab ca. 13:00 min, 16:00 min) https://sportdeutschland.tv/wwu-baskets-muenster/prob-nord-wwu-baskets-muenster-vs-art-giants-duesseldorf

## Rodmap
* Automatsiches ermitteln der Game-ID am Spieltag.

## Animiertes Schließen in Vmix
Mit diesem Beispielscript navigiert der Browser zu einer anderen URL, was dazu führt, dass eine Animation ausgeführt wird:

```
Function=BrowserNavigate&Input=1&Value=https%3A%2F%2Fbaskets.uber.space%2Flivestream-statistiken%2Fsingle-stats.html%3Fkey%3D<EUER_API_KEY>%26background-color%3Dtransparent%26%23none
Sleep 2000
Function=OverlayInput1Out
```
[Vmix Video](docs/vmix.mp4 "Demo.mp4")

## License
MIT License
